from api.models import Drone, Medication
from rest_framework import serializers
import re


class DroneSerializer(serializers.ModelSerializer):
    class Meta:
        model = Drone
        fields = '__all__'

    def validate_weight(self, value):
        if value > 500:
            raise serializers.ValidationError("The weight of the drone cannot be bigger than 500gr")
        elif value <= 0:
            raise serializers.ValidationError("The weight of the drone cannot be less than or equal to zero")
        return value

    def validate_battery(self, value):
        if value > 100 or value < 0:
            raise serializers.ValidationError(f"'{value}' is not a valid value for the battery")
        return value

class MedicationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Medication
        fields = '__all__'

    def validate_name(self, value):
        if not re.match("^[A-Za-z0-9_-]*$", value):
            raise serializers.ValidationError(f"'{value}' is not a valid value for the name")
        return value

    def validate_code(self, value):
        if not re.match("^[A-Z0-9_]*$", value):
            raise serializers.ValidationError(f"'{value}' is not a valid value for the code")
        return value

    def validate_weight(self, value):
        if value <= 0:
            raise serializers.ValidationError("The weight of the medication cannot be less than or equal to zero")
        return value