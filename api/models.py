from django.db import models

class Drone(models.Model):
    MODEL = [
        ('Lightweight', 'LW'),
        ('Middleweight', 'MW'),
        ('Cruiserweight', 'CW'),
        ('Heavyweight', 'HW')
    ]
    STATES = [
        ('IDLE', 'ID'),
        ('LOADING', 'LG'),
        ('LOADED', 'LD'),
        ('DELIVERING', 'DG'),
        ('DELIVERED', 'DD'),
        ('RETURNING', 'RG')
    ]
    serial_number = models.CharField(max_length=100, unique=True)
    model = models.CharField(max_length=30, choices=MODEL, default='Lightweight')
    weight = models.FloatField()
    battery = models.IntegerField()
    state = models.CharField(max_length=30, choices=STATES, default='IDLE')

    def get_weight(self):
        missions = Mission.objects.filter(drone=self.id)
        weight = sum([mission.item.weight for mission in missions])
        return weight

class Medication(models.Model):
    name = models.CharField(max_length=250)
    weight = models.FloatField()
    code = models.CharField(max_length=250, unique=True)
    image = models.URLField()


class Mission(models.Model):
    drone = models.ForeignKey(Drone, on_delete=models.CASCADE)
    item = models.ForeignKey(Medication, on_delete=models.CASCADE)
