from django.core.management.base import BaseCommand, CommandError
import datetime
import logging

from api.models import Drone

logger = logging.getLogger(__name__)

class Command(BaseCommand):
    help = 'Periodic task to check drones battery levels'

    def handle(self, *args, **options):
        self.stdout.write(self.style.SUCCESS(f"Cronjob Date: {datetime.date.today()}"))
        drones = Drone.objects.all()
        for drone in drones:
            if drone.battery < 25:
                self.stdout.write(self.style.NOTICE(f"The drone {drone.serial_number} is low on battery ({drone.battery}%)"))
            elif drone.battery <= 50:
                self.stdout.write(self.style.WARNING(f"The drone {drone.serial_number} has a good battery ({drone.battery}%)"))
            else:
                self.stdout.write(self.style.SUCCESS(f"The drone {drone.serial_number} has an excellent battery ({drone.battery}%)"))