from rest_framework.test import APITestCase
from django.urls import reverse
from api.models import Drone, Medication
import json


class DronesAPIViewTestCase(APITestCase):
    url = reverse("drone-list")

    def test_post_drones(self):
        response = self.client.post(self.url, {"serial_number": "DLW945", "model": "Lightweight", "weight": 500, "battery": 99})
        self.assertEqual(201, response.status_code)

    def test_get_drones(self):
        response = self.client.get(self.url)
        self.assertTrue(len(json.loads(response.content.decode('utf-8'))) == Drone.objects.count())


class LoadAPIViewTestCase(APITestCase):
    def url(self, serial):
        return reverse("load-drone", kwargs={"serial": serial})

    def test_invalid_loading(self):
        Drone.objects.create(serial_number='DLW369', weight=500, battery=99)
        Medication.objects.create(name='Aspirina-510', weight=510, code='ASP_510', image='http://images.com/asp_510.jpg')

        response = self.client.post(self.url('DLW369'), {"medication": "ASP_510"})
        self.assertEqual(400, response.status_code)