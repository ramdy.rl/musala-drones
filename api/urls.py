from django.urls import path, re_path

from api import views

urlpatterns = [
    path('drones/', views.ListCreateDroneAPIView.as_view(), name='drone-list'),
    path('medications/', views.ListCreateMedicationAPIView.as_view(), name='medication-list'),
    re_path('load_drone/(?P<serial>[\w-]+)/$', views.LoadDrone.as_view(), name='load-drone'),
    re_path('check_load/(?P<serial>[\w-]+)/$', views.CheckLoadDrone.as_view(), name='check-load'),
    re_path('check_availability/', views.CheckAvailableDrones.as_view(), name='check_availability'),
    re_path('check_battery/(?P<serial>[\w-]+)/$', views.CheckBatteryDrone.as_view(), name='check_battery')
]