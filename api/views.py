from rest_framework.generics import ListCreateAPIView
from api.models import Drone, Medication, Mission
from api.serializers import DroneSerializer, MedicationSerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status


class ListCreateDroneAPIView(ListCreateAPIView):
    serializer_class = DroneSerializer
    queryset = Drone.objects.all()

class ListCreateMedicationAPIView(ListCreateAPIView):
    serializer_class = MedicationSerializer
    queryset = Medication.objects.all()

class LoadDrone(APIView):
    def post(self, request, serial):
        try:
            med = Medication.objects.get(code=request.data.get('medication'))
            if med.weight > 500:
                return Response({"This weight cannot be shipped"}, status=status.HTTP_400_BAD_REQUEST)
        except Medication.DoesNotExist:
            return Response({"The medication doesn't exists"}, status=status.HTTP_404_NOT_FOUND)
        try:
            drone = Drone.objects.get(serial_number=serial)
            if drone.battery < 25:
                drone.state = 'IDLE'
                drone.save()
                return Response({f"The drone {drone.serial_number} is low on battery ({drone.battery}%)"}, status=status.HTTP_400_BAD_REQUEST)
            if (drone.get_weight() + med.weight) <= drone.weight:
                try:
                    Mission.objects.get(drone=drone, item=med)
                except Mission.DoesNotExist:
                    Mission.objects.create(drone=drone, item=med)
                    if drone.get_weight() < drone.weight:
                        drone.state = 'LOADING'
                    elif drone.get_weight() == drone.weight:
                        drone.state = 'LOADED'
                    drone.save()
                return Response({f"The medication {med.code} was asignated to the drone {drone.serial_number}"} , status=status.HTTP_201_CREATED)
            else:
                return Response({f"The medication {med.code} cannot be asignated to the drone {drone.serial_number}"},
                                status=status.HTTP_400_BAD_REQUEST)
        except Drone.DoesNotExist:
            return Response({"The drone doesn't exists"}, status=status.HTTP_404_NOT_FOUND)

class CheckLoadDrone(APIView):
    def get(self, request, serial):
        try:
            drone = Drone.objects.get(serial_number=serial)
            missions = Mission.objects.filter(drone=drone.id)
            items = [mission.item.name for mission in missions]
            return Response(data=items, status=status.HTTP_200_OK)
        except Drone.DoesNotExist:
            return Response({"The drone doesn't exists"}, status=status.HTTP_404_NOT_FOUND)

class CheckAvailableDrones(APIView):
    def get(self, request):
        try:
            drones = Drone.objects.filter(state__in=['IDLE', 'LOADING'], battery__gt=25)
            items = [{"serial_number": drone.serial_number, "current_weight": drone.get_weight(),
                      "weight_limit": drone.weight, "battery": drone.battery} for drone in drones]
            return Response(data=items, status=status.HTTP_200_OK)
        except Drone.DoesNotExist:
            return Response({"The drone doesn't exists"}, status=status.HTTP_404_NOT_FOUND)

class CheckBatteryDrone(APIView):
    def get(self, request, serial):
        try:
            drone = Drone.objects.get(serial_number=serial)
            return Response(data={"battery": f"{drone.battery}%"}, status=status.HTTP_200_OK)
        except Drone.DoesNotExist:
            return Response({"The drone doesn't exists"}, status=status.HTTP_404_NOT_FOUND)

