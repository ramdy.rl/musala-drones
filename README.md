## Installation
The first thing to do is to create a virtual environment and install the dependencies.
```
https://virtualenv.pypa.io/en/latest/user_guide.html

```

Clone the project, move to the directory project and with the virtualenv active, run
```
pip install -r requirements.txt
```
It's not necessary to run `python manage.py migrate` because the database is local (db.sqlite3) and it's already populated

## Structure

Endpoint | HTTP Method | Result

`drones` | GET | Get all drones

`drones` | POST | Create a new drone

`medications` | GET | Get all medications

`medications` | POST | Create a new medication

`load_drone/:serial` | POST | Load a drone with medication items

`check_load/:serial` | GET | Check loaded medication items for a given drone

`check_availability/` | GET | Check available drones for loading

`check_battery/:serial` | GET | Check drone battery level for a given drone


## Use
We can test the API using [curl](https://curl.haxx.se/) or [httpie](https://github.com/jakubroztocil/httpie#installation), or we can use [Postman](https://www.postman.com/)

Start up Django's development server.
```
python manage.py runserver
```
The home page of the project contains the description of the API endpoints
```
http://127.0.0.1:8000/
```

## Example
To load the drone endpoint, it is necessary to specify the drone serial number in the url and pass the medication code by POST 
```
curl -X POST "http://127.0.0.1:8000/api/load_drone/DLW128/" -H "accept: application/json" -H  "Content-Type: application/json" -d "{\"medication\":  \"ASP_100\"}"
```
Result
[
    "The medication ASP_100 was asignated to the drone DLW128"
]

## Running a task (cronjob)

To check drones battery levels there is a command named check_battery that can be executed manually
 
python manage.py check_battery
 
If this task should be run as a periodic task, set the crontab file

Change the check_battery.sh file with the path of your virtualenv and your project, and edit the crontab file to run check_battery.sh on the date you want
 
crontab -e

@daily /path/to/project/check_battery.sh > /path/to/logs/check_battery.log


## Tests

To run tests, run the command 

python manage.py test
 
 