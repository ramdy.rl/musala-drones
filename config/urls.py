from django.urls import path, include
from rest_framework_swagger.views import get_swagger_view

schema_view = get_swagger_view(title='Drones API')

urlpatterns = [
    path('', schema_view),
    path('api-auth/', include('rest_framework.urls')),
    path('api/', include('api.urls'))
]
